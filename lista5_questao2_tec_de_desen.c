#include <stdio.h>
#include <stdlib.h>

/* 
Escreva um trecho de c�digo em C para fazer a cria��o dos novos tipos de dados conforme solicitado abaixo:
Hor�rio: composto de hora, minutos e segundos.
Data: composto de dia, m�s e ano.
Compromisso: composto de uma data, hor�rio e texto que descreve o compromisso.
*/
struct horario {
  int hora;
  int minutos;
  int segundos;
};
struct data {
  char dia;
  char mes[12];
  char ano;
};
struct compromisso {
  struct data date;
  struct horario time;
  char texto[256];
};
int main() {
	return 0;
}
