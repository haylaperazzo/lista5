#include <stdio.h>
#include <stdlib.h>

/*

Uma empresa comercial possui um programa para controle das receitas e despesas em seus 10 projetos. 
Os projetos sao numerados de 0 ate 9. 
Faca um programa C que controle a entrada e saida de recursos dos projetos. 
O programa devera ler um conjunto de informacoes contendo:
Numero do projeto, valor, tipo despesa ("R" - Receita e "D" - Despesa). 
O programa termina quando o valor do codigo do projeto for igual a -1. 
Sabe-se que Receita deve ser somada ao saldo do projeto e despesa subtraida do saldo do projeto. 
Ao final do programa, imprimir o saldo final de cada projeto.

*/

struct project {
	
int num;
float valor;
char tipo;

} project;

int main() {
	
	int i;
	float projetos[10] = {0};
	
	printf("Digite o numero do projeto ou -1 para sair: ");
	scanf("%d", &project.num);

	while (project.num != -1) {
			printf("Digite o Valor : ");
			scanf("%f", &project.valor);
			printf("Digite R para Receita ou D para Despesa: ");
			getchar();
			scanf("%c", &project.tipo);

	if (project.tipo == 'r' || project.tipo == 'R') {
			projetos[project.num] = projetos[project.num] + project.valor;
	
	} else {
		  if (project.tipo == 'D' || project.tipo == 'd') {
					projetos[project.num] = projetos[project.num] - project.valor;
					
		   } else {
					printf("Opcao Invalida");
						
		    }
	   }
		printf("Digite o numero do projeto ou -1 para sair: ");
		scanf("%d", &project.num);
	}
	printf("\n");
	
	for (i = 0; i < 10; i++) {
			printf("Saldo Final %d: R$ %.2f\n", i, projetos[i]);
	
	}
	return 0;
}

