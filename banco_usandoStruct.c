#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* 
Considere a estrutura. Fa�a um programa com os requisitos:

a) Cria uma conta

b) Consulta o saldo do cliente (entra com o numero da conta conferindo apenas a senha)

c) Deposita um valor (entra com o numero da conta e confere o nome do cliente)

d) Saca um valor (entra com o numero da conta e confere senha e chave, o cliente tem apenas autoriza��o de sacar o seu dinheiro, conta sem limite)

e) Encerre a conta ( entra com o numero da conta e confere senha e chave, fa�a uma pergunta para que o cliente confira a opera��o e apague seus dados)

*/

// yay!!!
void criarConta();
void saldoConta();
void depositarConta(); 
void saqueConta(); 
void encerrarConta();

void showHeader(char *title);
int getConta();
int validarSenha(int pos);
int validarChave(int pos);
int validarNome(int pos);
void showConta(int pos);

int strIsLower(char *string);
int strIsDigits(char *string);


#define MAX_CONTAS 200
#define AVAILABLE -1

struct banco {
  int conta;  
  float saldo; 
  float depositar;
  float saque;
  int ecerrar;
  char senha[5];	// 4 d�gitos + '\0'
  char chave[4]; 
  char nome[100]; 
} dado[MAX_CONTAS];

int todo_contas;
int proxima_conta = 101;

void init() {
	int i;
	todo_contas = 0;
	for(i=0; i < MAX_CONTAS; i++) {
		dado[i].conta = AVAILABLE;
	}
}


int main() {
	init();
	
	int escolha = 0; 
    while(escolha != 6) { 
		system("@cls||clear");
	    printf("\n*********************"); 
	    printf("\n Pig Bank "); 
	    printf("\n*********************"); 
	    printf("\n1-Criar Conta"); 
	    printf("\n2-Saldo"); 
	    printf("\n3-Deposito"); 
	    printf("\n4-Saque");
	    printf("\n5-Encerrar Conta");
	    printf("\n6-Sair"); 
	    printf("\nEnter your choice: "); 
	    scanf(" %d",&escolha);

		switch(escolha) { 
            case 1: 
            	criarConta();
            	break; 
                
            case 2: 
				saldoConta(); 
            	break; 
                
            case 3: 
				depositarConta(); 
            	break; 
                
            case 4: 
				saqueConta(); 
            	break;

            case 5: 
				encerrarConta(); 
            	break;  
                
            case 6:
            	printf("Sair\n");
            	break; 

            default: 
				printf("Digite de 1-6\n");
				break;
		} 
		system("pause"); 
    } 
    
    return 0;
} 

// Criar uma conta
void criarConta() { 
	char buffer[1024];
	int pos = todo_contas++;
	int conta = proxima_conta++;

    showHeader("Criar Conta");
     
	printf("\nSeu numero da conta e : %d", conta); 
	dado[pos].conta = conta; 

	printf("\nDigite seu nome (max 99): "); 
    scanf(" %99[^\n]",buffer);
	strcpy(dado[pos].nome, buffer); 
	printf("\nSeu nome: %s", dado[pos].nome); 
	
	do  {
		printf("\nDigite sua senha (4 digite [0-9]): "); 
	    scanf(" %s",buffer);
		if(strlen(buffer) != 4 || !strIsDigits(buffer)) {
			printf("\nSenha Invalida"); 
		}
	} while(strlen(buffer) != 4 || !strIsDigits(buffer));
	strcpy(dado[pos].senha, buffer);
	printf("\nSua senha: %s", dado[pos].senha); 

	do  {
		printf("\nDigite sua chave (3 digite [a-z]): ");  
	    scanf(" %s",buffer);
		if(strlen(buffer) != 3 || !strIsLower(buffer)) {
			printf("\nChave Invalida"); 
		}
	} while(strlen(buffer) != 3 || !strIsLower(buffer));
	strcpy(dado[pos].chave, buffer);
	printf("\nSua chave: %s", dado[pos].chave); 

	dado[pos].depositar = 0.0;
	dado[pos].saque = 0.0;
	dado[pos].saldo = dado[pos].depositar - dado[pos].saque; 

	printf("\n%s, Seu minimo deposito e de %.2f\n", dado[pos].nome, dado[pos].saldo); 
	
	dado[pos].ecerrar = 0;	// falso
	
	printf("\n");

} 


// Consulta o saldo do cliente (entra com o numero da conta conferindo apenas a senha)
void saldoConta() { 
    int pos;
    showHeader("Saldo");
    pos = getConta();
	pos = validarSenha(pos);
	showConta(pos);
}

//Deposita um valor (entra com o numero da conta e confere o nome do cliente)
void depositarConta() {
    int num, pos, i;
    float quanto;
    
    showHeader("Depositar");
    pos = getConta();
    pos = validarNome(pos);

	// depositar
	showConta(pos);
    if(pos >= 0) { 
		printf("\n Quanto deseja depositar dgora?"); 
		scanf("%f",&quanto); 
		dado[pos].depositar += quanto; 
		dado[pos].saldo = dado[pos].depositar - dado[pos].saque; 
    	printf("\n Valor do deposito: %.2f", quanto); 
    	printf("\n Saldo Atual: %.2f",dado[pos].saldo); 
	} 
	printf("\n");
} 

// Saca um valor (entra com o numero da conta e confere senha e chave, 
//	o cliente tem apenas autoriza��o de sacar o seu dinheiro, conta sem limite)
void saqueConta() { 
    int num, pos, i;
    float quanto;
    
    showHeader("Saque");
    pos = getConta();
    pos = validarSenha(pos);
    pos = validarChave(pos);

	// saque
	showConta(pos);
	
    if(pos >= 0) { 
    	printf("\n Quanto deseja sacar?"); 
    	scanf("%f",&quanto);

		if(dado[pos].saldo - quanto < 0.0) {
		    printf("\n Saque nao foi efetuado sevido a saldo insuficiente "); 
		} else {
			dado[pos].saque += quanto; 
			dado[pos].saldo = dado[pos].depositar - dado[pos].saque; 
	    	printf("\n Quantidade retirada : %.2f", quanto); 
		}
    	printf("\n Saldo atual: %.2f",dado[pos].saldo); 
	} 
	printf("\n");
} 

// Encerre a conta ( entra com o numero da conta e confere senha e chave, 
// fa�a uma pergunta para que o cliente confira a opera��o e apague seus dados)
void encerrarConta() {
    int num, pos, i;
    char confirme;
    
    showHeader("Encerrar Conta");
    pos = getConta();
    pos = validarSenha(pos);
    pos = validarChave(pos);

	showConta(pos);

	// encerrar
    if(pos >= 0) { 
    	if(dado[pos].saldo == 0) {
	    	printf("\n Voce confirma que quer encerrar conta? [s/n]: "); 
	    	scanf(" %c", &confirme);
	    	if(tolower(confirme) == 's') {
				dado[pos].ecerrar = 1; 
		    	printf("\n Conta Fechada");
			} else {
				printf("\n Conta nao Fechada");
			}
		} else {
	    	printf("\n Nao e possivel encerrar sua conta com saldo maior que zero.");
		}
	} 
	printf("\n");
}

void showHeader(char *title) {
    printf("\n*************************************"); 
    printf("\n %s ", title); 
    printf("\n*************************************"); 
}

int getConta() {
	char buffer[50];

	int pos = -1;
	int i, num, senha;

    printf("\nDigite seu numero da conta: "); 
    scanf(" %s",buffer);
    num = atoi(buffer);
    
    // find account
    pos = -1;
    for(i=0; i<todo_contas; i++) {
		if(dado[i].conta == num) {
        	pos = i;
			break;
		}
	}

    if(pos >= 0 && dado[pos].ecerrar == 1) { 
    	printf("\n Conta Fechada\n");
    	pos = -1;
	}
	
	return pos;	
}

int validarSenha(int pos) {
	char buffer[50];

	if(pos >= 0 && pos < MAX_CONTAS) {
		printf("\nDigite sua senha: "); 
	    scanf(" %s",buffer);
	
		if(strcmp(dado[pos].senha, buffer) != 0) {
			printf("\nSenha Invalida"); 
			pos = -1;
		}
	}
	return pos;
}

int validarChave(int pos) {
	char buffer[50];

	if(pos >= 0 && pos < MAX_CONTAS) {
		printf("\nDigite sua chave: "); 
	    scanf(" %s",buffer);
	
		if(strcmp(dado[pos].chave, buffer) != 0) {
			printf("\nChave Invalida"); 
			pos = -1;
		}
	}
	return pos;
}

int validarNome(int pos) {
	char buffer[100];

	if(pos >= 0 && pos < MAX_CONTAS) {
		printf("\nDigite seu nome: "); 
	    scanf(" %99[^\n]",buffer);
	
		if(strcmp(dado[pos].nome, buffer) != 0) {
			printf("\nNome Invalido"); 
			pos = -1;
		}
	}
	return pos;
}

void showConta(int pos) {
	if(pos >= 0 && pos < MAX_CONTAS) {
    	printf("\n Numero da conta : %d",dado[pos].conta); 
    	printf("\n Nome : %s",dado[pos].nome); 
    	printf("\n Saldo : %.2f",dado[pos].saldo); 
    } else { 
      	printf("\n Conta Invalida!\n"); 
	} 
	printf("\n");
}

int isStringLowercase(char *string) {
	int eLower = 1;	// verdade
	int i = 0;
	while(string[i] != '\0') {
		if(string[i] < 'a' || string[i] > 'z') {
			eLower = 0; // falso
			break;
		}
		i++;
	}
	return eLower;
}

int strIsLower(char *string) {
	int result = 1;	// verdade
	int i = 0;
	while(string[i] != '\0') {
		if(string[i] < 'a' || string[i] > 'z') {
			result = 0; // falso
			break;
		}
		i++;
	}
	return result;
}

int strIsDigits(char *string) {
	int result = 1;	// verdade
	int i = 0;
	while(string[i] != '\0') {
		if(string[i] < '0' || string[i] > '9') {
			result = 0; // falso
			break;
		}
		i++;
	}
	return result;
}


